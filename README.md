# wether

### Project setup
```
yarn install
```

###Vue CLI 3 install
-> https://vuetifyjs.com/ru/getting-started/quick-start
```
vue add vuetify
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
